function [sCalendar,tempTable]=OnCallScheduler
worstDates=[]; % none yet
[RATable,BadDays,BadDates]=loadRATable(worstDates);

% setup calendar
[numeric,text,raw]=xlsread('onCall.xls','Date Table');
StartDate=datenum(raw{1,2});
EndDate=datenum(raw{2,2});
BigDays=processDates(raw{3,2})
NullDays=processDates(raw{4,2})
SHLDays=split(upper(raw{5,2}))
WeekdayNum=raw{6,2}
WeekendNum=raw{7,2}

sCalendar=[StartDate:EndDate];
badDex=[NullDays-StartDate+1];
sCalendar(badDex)=[];
sCalendar(2,1)=0; % difficulty tab
for k=1:size(sCalendar,2)
    cDay=sCalendar(k);
    cDOW=upper(datestr(cDay,'ddd'));
    sCalendar(2,k)=sCalendar(2,k)+1.3*sum(strcmp({'FRI','SAT'},cDOW));
    sCalendar(2,k)=sCalendar(2,k)+1*sum(strcmp(BadDays,cDOW));
    sCalendar(2,k)=sCalendar(2,k)+2*length(find(BadDates==cDay));
end
didFail=1;
attempts=0;
while didFail
    didFail=0;
    sCalendar(2,:)=sCalendar(2,:)+1.4*(rand(size(1,size(sCalendar,2)))-.7);

    [junk,reMap]=sort(1-sCalendar(2,:)); % reorder by difficulty

    sCalendar=sCalendar(:,reMap);
    tempTable=RATable;
    for k=1:size(sCalendar,2)
        cDay=sCalendar(1,k);
        cDOW=upper(datestr(cDay,'ddd'));
        isWE=sum(strcmp({'SAT','SUN'},cDOW))>0 | length(find(cDay==BigDays))>0;
        isSHL=sum(strcmp(SHLDays,cDOW))>0;
        if isWE
            [tempTable,raToday]=pickRA(tempTable,cDay,isSHL,isWE,WeekendNum);
        else
            [tempTable,raToday]=pickRA(tempTable,cDay,isSHL,isWE,WeekdayNum);
        end
        if isempty(raToday)
            sCalendar(2,k)=sCalendar(2,k)+1; % this day was harder than it looked
            didFail=1;
            break;
        end
        if length(raToday)<2
            raToday(2)=0;
        end
        sCalendar(3:4,k)=raToday';


    end
    if didFail & attempts>30 % acknowledge fewer of the desired days off
        [junk,reMap]=sort(1-sCalendar(2,:)); % reorder by difficulty

        sCalendar=sCalendar(:,reMap);
        wLen=length(worstDates);
        worstDates;
        k=1;
        while length(worstDates)==wLen & k<=size(sCalendar,2)
            if length(find(worstDates==sCalendar(1,k)))==0
                worstDates=[worstDates sCalendar(1,k)];
                disp(['Prefered Bad Day : ' datestr(sCalendar(1,k),2) ' is no longer an option'])
            end
            k=k+1;
        end

        [RATable,BadDays,BadDates]=loadRATable(worstDates);
        attempts=0;
    end
    if didFail
        attempts=attempts+1
    end
end

[junk,reMap]=sort(sCalendar(1,:));
sCalendar=sCalendar([1,3,4],reMap);
doIcal(sCalendar,tempTable,BigDays); % write ical file
doGCal(sCalendar,tempTable,BigDays); % write gcal file
length(sCalendar(1,:))
calTxt='';
lasMonth='Bub';
for k=1:length(sCalendar(1,:))
    cDay=sCalendar(1,k);
    cMonth=datestr(cDay,'mmm');
    if strcmp(cMonth,lasMonth)
        % same month
        dayOfMonth=cDay-datenum([cMonth '-00']);
        [a,b]=find(kCal==dayOfMonth);
        %{RATable(sCalendar([3,4],k)).name}
        try
            curCal{a,b}={sCalendar([2,3],k)};
        catch
            curCal{a,b}={sCalendar([2],k)}; % only one RA
        end

    else
        % new month

        if ~strcmp(lasMonth,'Bub')
            calTxt=[calTxt printCal(kCal,curCal,lasMonth,RATable)];
        end
        kCal=calendar(cDay);
        curCal={};
        lasMonth=cMonth;
        dayOfMonth=cDay-datenum([cMonth '-00']);
        [a,b]=find(kCal==dayOfMonth);
        %{RATable(sCalendar([3,4],k)).name}
        % try
        curCal{a,b}={sCalendar([2,3],k)};
        % catch
        %     curCal{a,b}={RATable(sCalendar([2],k)).name}; % only one RA
        % end
    end
end
if ~isempty(curCal)
    calTxt=[calTxt printCal(kCal,curCal,lasMonth,RATable)];
end
fid = fopen('chart.html','wt');
fprintf(fid,calTxt);
nTxt=['<center><table><tr><td>RA</td><td>End of Semester Points</td></tr>'];
k1={tempTable.name};k2=[tempTable.points];
for i=1:length(k1)
    nTxt=[nTxt '<tr><td>' k1{i} '</td><td>' num2str(k2(i)) '</td></tr>'];
end
nTxt=[nTxt '</table></center>'];
fprintf(fid,nTxt);
fclose(fid);
k1={tempTable.name};k2=[tempTable.points];
for i=1:length(k1)
    disp([k1{i} ':' num2str(k2(i))]);
end



function [tempTable,raToday]=pickRA(tempTable,cDay,shlTag,isWE,numPick)
cDOW=upper(datestr(cDay,'ddd'));
global tPoints;
eligibleList=[];
for k=1:length(tempTable)

    if sum(strcmp(tempTable(k).BadDays,cDOW))>0 | length(find(tempTable(k).BadDates==cDay))>0
        % you are not eligible
    else
        % you are eligble
        eligibleList(:,end+1)=[k;(tempTable(k).Weighting*tempTable(k).points)];
    end
end
ptsToday=1;
if isWE % is 24hour
    ptsToday=ptsToday+1;
end
if strcmp(cDOW,'FRI')
    ptsToday=ptsToday+.5;
end
tPoints=tPoints+ptsToday;
raToday=[];
% fewest points first
if length(eligibleList)>numPick
    [junk,reMap]=sort(eligibleList(2,:));
    nMap=eligibleList(1,reMap);
    if shlTag
        raToday=[];k=1;
        while length(raToday)<1
            if tempTable(nMap(k)).SHLTag==1
                raToday(1)=nMap(k);
                nMap(k)=[];
            end
            k=k+1;
            if k>length(nMap) & length(raToday)<1
                disp('Shitte')
                return;
            end
        end

        raToday(2:numPick)=nMap(1:(numPick-1));
    else
        raToday=nMap(1:numPick);
    end
    if length(raToday)>1
        if sum(strcmp(tempTable(raToday(1)).BadMorgen,cDOW))>0 % today is bad morgan
            iy=2;
            while iy<=length(raToday)
                if sum(strcmp(tempTable(raToday(iy)).BadMorgen,cDOW))>0 % bad morning
                    raToday(iy)=[];
                else
                    iy=iy+1;
                end
            end
        end
        if sum(strcmp(tempTable(raToday(1)).BadAbend,cDOW))>0 % today is bad abend
            iy=2;
            while iy<=length(raToday)
                if sum(strcmp(tempTable(raToday(iy)).BadAbend,cDOW))>0 % bad morning
                    raToday(iy)=[];
                else
                    iy=iy+1;
                end
            end
        end
    end
    for k=1:length(raToday)
        tempTable(raToday(k)).points=tempTable(raToday(k)).points+ptsToday;
    end
else
    raToday=[];
end
if length(raToday)<numPick
    raToday=[];
end

function dateNums=processDates(dasStr)
dateNums=[];
if length(dasStr)>1
    ndasStr=split(dasStr);
    for i=1:length(ndasStr)
        try
            cDate=datenum(ndasStr{i});
            dateNums(end+1)=cDate;
        catch
            disp(['Bad Date ' ndasStr{i}]);
        end
    end
end

function calTxt=printCal(kCal,curCal,lasMonth,RATable)
colorTable={'FFFFFF','FF0000','00FF00','0000FF','CCFF00','00CCCC','CC00CC','FF9933','CCFF00','FF00CC','00FFCC','00CCFF'};
colorTable={'FFFFFF','FF0000','00FF00','0000FF','CCFF00','00CCCC','CC00CC','FF9933','CCFF00','FF00CC','00FFCC','00CCFF','CC0000','00CC00','0000CC','CCCC00','0099CC','CC33CC','CC9933','CC66FF','FF99CC','00FFCC','33FFCC'};
calTxt=['<bold><center>' datestr(datenum([lasMonth '07']),'mmmm') '</center></bold><table border="2" width="100%%">'];
calTxt=[calTxt '<tr><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>'];
curCal{size(kCal,1),size(kCal,2)}=[];
for i=1:size(kCal,1)
    calTxt=[calTxt '<tr>'];
    for j=1:size(kCal,2)
        curTxt='';
        if ~isempty(curCal{i,j})
            curTxt=['<center><table border="1" width="100%%"><tr><td bgcolor="000000"><bold><center><font color="FFFFFF">' num2str(kCal(i,j)) '</font></center></bold></td></tr>'];
            curNames=curCal{i,j}{1};

            for k=1:length(curNames)

                curTxt=[curTxt '<tr><td bgcolor="' colorTable{curNames(k)} '">' RATable(curNames(k)).name '</td></tr>'];
            end
            curTxt=[curTxt '</table></center>'];
        end
        calTxt=[calTxt '<td>' curTxt '</td>'];

    end
    calTxt=[calTxt '</tr>'];
end
calTxt=[calTxt '</table><hr>'];
function celstr=split(dasStr,splitChar)
dasStr(find(dasStr==' '))=[];
if nargin<2
    splitChar=',';
end
sptLoc=find(dasStr==splitChar);
if length(sptLoc)>0
    celstr{1}=dasStr(1:(sptLoc(1)-1));
    for i=2:length(sptLoc)
        celstr{i}=dasStr((sptLoc(i-1)+1):(sptLoc(i)-1));
    end

    celstr{end+1}=dasStr((sptLoc(end)+1):end);
else
    celstr{1}=dasStr;
end


function [RATable,BadDays,BadDates]=loadRATable(worstDates)
[numeric,text,raw]=xlsread('onCall.xls','RA Table');
% setup RA Table

RATable={};
BadDays=['Non'];
BadDates=[];
wrstList=[];
for i=2:length(raw(:,1))
    RATable(i-1).name=raw{i,1};
    RATable(i-1).points=raw{i,2};
    RATable(i-1).SHLTag=raw{i,3};
    RATable(i-1).Weighting=raw{i,4};
    RATable(i-1).BadDays=split(upper(raw{i,5}));
    RATable(i-1).BadMorgen=split(upper(raw{i,5}));
    RATable(i-1).BadAbend=split(upper(raw{i,5}));
    BadDays=[BadDays ',' upper(raw{i,5})];
    bDates=processDates(raw{i,6});
    RATable(i-1).BadDates=bDates;
    sbDates=processDates(raw{i,8}); % sort of bad dates are bad dates but if there is a really bad day they will be fixed
    for j=1:length(worstDates)
        ccEl=find(sbDates==worstDates(j));
        if ~isempty(ccEl)
            % randomize the whole deal
            %sbDates(ccEl)=[]; % delete worst elements
            wrstList(end+1,:)=[j i-1 ccEl(1)+length(RATable(i-1).BadDates)];
        end

    end
    RATable(i-1).BadMorgen=split(upper(raw{i,9}));
    RATable(i-1).BadAbend=split(upper(raw{i,10}));
    RATable(i-1).BadDates(end+1:end+length(sbDates))=sbDates;
    BadDates(end+1:end+length(sbDates))=sbDates;

end
for j=1:length(worstDates)
    if ~isempty(wrstList)
        wList=wrstList(find(wrstList(:,1)==j),:);
        if size(wList,1)>0
            delEle=1+round(rand*(length(wList)-1)); % some stochastics
            RATable(wList(delEle,2)).BadDates(wList(delEle,3))=[];
        end
    end
end
BadDays=split(BadDays);



function doIcal(sCalendar,tempTable,BigDays)
nStr='BEGIN:VCALENDAR#VERSION:2.0#X-WR-CALNAME:OnCall#PRODID:-//KevinCal, Inc#CALSCALE:GREGORIAN#';

nStr(find(nStr=='#'))=sprintf('\n');
mk=fopen('outCal.ics','w');
fprintf(mk,nStr);
for k=1:size(sCalendar,2)
    cDay=sCalendar(1,k);
    cDOW=upper(datestr(cDay,'ddd'));
    isWE=sum(strcmp({'SAT','SUN'},cDOW))>0 | length(find(cDay==BigDays))>0;
   
    if isWE
        cDay=cDay+9/24;
        dur='24';
    else
        cDay=cDay+17/24;
        dur='16';
    end
    cDay=cDay+4/24;
    for nn=2:3
    cRA=sCalendar(nn,k);

    eStr='BEGIN:VEVENT\n';
    eStr=[eStr 'DTSTART;TZID=US/Eastern:' datestr(cDay,30) 'Z\n'];
    eStr=[eStr 'SUMMARY:On Call for ' tempTable(cRA).name '\n'];
    eStr=[eStr 'UID:71730FB0-D1CD-413E-B38E-CAD28D58BAEE.' num2str(2*k-(3-nn)) '\n'];
    eStr=[eStr 'DTSTAMP:' datestr(now,30) 'Z\n'];
    %eStr=[eStr 'SEQUENCE:2\n'];
    eStr=[eStr 'DURATION:PT' dur 'H\n'];
    eStr=[eStr 'END:VEVENT\n'];
    fprintf(mk,eStr);
    end
end
fprintf(mk,'END:VCALENDAR');
fclose(mk);


function doGCal(sCalendar,tempTable,BigDays)

mk=fopen('outCal.csv','w');
nStr=['Subject, Start,End\n'];

fprintf(mk,nStr);

for k=1:size(sCalendar,2)
    cDay=sCalendar(1,k);
    cDOW=upper(datestr(cDay,'ddd'));
    isWE=sum(strcmp({'SAT','SUN'},cDOW))>0 | length(find(cDay==BigDays))>0;
    
    if isWE
        cDay=cDay+9/24;
        bonD=' (24)';
        dur=24;
    else
        cDay=cDay+17/24;
        dur=16;
        bonD='';
    end
    
    for nn=2:3
        cRA=sCalendar(nn,k);
        eStr=[tempTable(cRA).name bonD ',' datestr(cDay,21) ' , ' datestr(cDay+dur/24,21) ''];
        eStr=[eStr ''  '\n'];

    fprintf(mk,eStr);
    end
  
end
%fprintf(mk,'END:VCALENDAR');
fclose(mk);